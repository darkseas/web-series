{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Backend Twin Architecture\n",
    "\n",
    "This is the final tutorial of the Backend Series detailing the architecture of the Bayu Digital Twin.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recap\n",
    "\n",
    "So by now you will know about\n",
    "* the HTTP request-response cycle\n",
    "* common Django structure\n",
    "* the more specific Django-REST structure\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview\n",
    "\n",
    "The ground covered here will be\n",
    "* Django app - `api`\n",
    "  + urls\n",
    "  + models\n",
    "  + views\n",
    "* COP_2001 module\n",
    "  + module structure - `data`, `figure`, `json_safe`\n",
    "  + example `tension`\n",
    "* Add new module\n",
    "  + write a new module\n",
    "  + run the module tests\n",
    "  + plumb it in\n",
    "  + test from the browser"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Objectives\n",
    "\n",
    "By the end of tutorial, you should be able to:\n",
    "\n",
    "1. Describe the request-response cycle in the Bayu Twin.\n",
    "2. Make a new plotting module\n",
    "3. Add new module to the web-app\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Skip this tutorial if..\n",
    "\n",
    "If you have\n",
    "- made django web apps\n",
    "- used the Django-REST framework or similar json base"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## External Resources\n",
    "\n",
    "Django Framework - The central tutorial is very good.\n",
    "- Tutorial https://docs.djangoproject.com/en/3.1/intro/tutorial01/\n",
    "- Models and fields https://docs.djangoproject.com/en/3.1/topics/db/models/\n",
    "- URLS https://docs.djangoproject.com/en/3.1/topics/http/urls/\n",
    "- Static files https://docs.djangoproject.com/en/3.1/howto/static-files/\n",
    "\n",
    "Django REST - questionable value, lots of magic you don't need.\n",
    " - Views https://www.django-rest-framework.org/api-guide/views/\n",
    " - Serializers https://www.django-rest-framework.org/api-guide/serializers/\n",
    " \n",
    "Bayu Twin - Bitbucket links to the source code\n",
    "- API urls - https://bitbucket.org/seassociates/cop-2001/src/dev/project/api/urls.py\n",
    "- API views - https://bitbucket.org/seassociates/cop-2001/src/dev/project/api/views.py\n",
    "- Plotting modules - https://bitbucket.org/seassociates/cop-2001/src/dev/cop_2001/\n",
    "- Example plotting module `tension.py` https://bitbucket.org/seassociates/cop-2001/src/dev/cop_2001/tension.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Django App - `api`\n",
    "\n",
    "Recall that the base of a django project is the directory that contains `manage.py`. One Django\n",
    "project may contain many apps to do different things; user management, file uploads, and so on.\n",
    "\n",
    "The main *app* in Bayu is `api`, which is the app which serves JSON to api calls. There is a small\n",
    "app `bayu` as well, which serve the base template that the Javascript front-end is rendered into.\n",
    "The `project` app (inside the `project` project) is where the first url paths are stored.\n",
    "\n",
    "```\n",
    "Project `project`\n",
    "+- Dir `settings`\n",
    "   +- base.py \n",
    "   +- development.py \n",
    "   +- production.py \n",
    "\n",
    "+- Dir 'static' - location of the javascript files\n",
    "\n",
    "+- App `project`\n",
    "   +- settings.py *ignore* (note the zero-bomb on Line 22 or so)\n",
    "   +- urls.py - This is the root url conf, defaults to bayu app.\n",
    "\n",
    "+- App `bayu`\n",
    "   +- views.py - Index view and login decorator for whole site\n",
    "   +- Dir templates - base and child templates for whole site\n",
    "\n",
    "+- App `api`\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![plotting-structure](assets/img/module-structure.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structure of a plotting module\n",
    "\n",
    "The most numerous code module, written by the most number of people, will be the plotting module. These are modules (but could be objects) which have three common functions:\n",
    "\n",
    "1. `data(KP=None)` which takes the KP, and returns any and all data that your plotting function will need.\n",
    "2. `json_safe(data)` which guarantees to make the `data` into an an object that can be seralized into JSON safely.\n",
    "3. `figure(data)` which will take the data from the `data` function and return a Bokeh object that can be embedded, typically a Figure, but also a DataTable.\n",
    "\n",
    "![plotting-structure](assets/img/plotting-module.png)\n",
    "\n",
    "### `data(KP=None) -> data_dict`\n",
    "\n",
    "The call for the data is separated so that the data can be stored, and also sent direct to the browser if required. Initial plans by TheCut had the Bokeh plotting occurring in the browser, and having the data-as-a-service allows it to be used by other widgets, plots or tables.\n",
    "\n",
    "The structure of the data returned does not really matter, as long as your `json_safe` function and `figure` function can handle it. The simplest structure is probably a DataFrame, more common is a dictionary with dataframes and other details like `kp_start` and `kp_end`.\n",
    "\n",
    "### `json_safe(data) -> data_dict`\n",
    "\n",
    "The `json_safe` function needs to be written to convert any datastructures from `data` into objects that can be serialized. JSON is actually text, but this function *does not return text* but rather vanilla datastructures that are easily serialised.\n",
    "\n",
    "For example, a DataFrame is not natively serializable, but can be turned into a series of dictionaries with `df.to_dict('records')`. The data will be stored in JSON field in the database, so needs to be JSON-safe or errors will occur.\n",
    "\n",
    "\n",
    "### `figure(data) -> Bokeh Figure`\n",
    "\n",
    "This function returns a Bokeh Figure usually, that `bokeh` will serialise and embed itself. The object returned by `figure` will be embedded with `bokeh.embed.json_item`, so as long at this occurs successfully, you're good. Another object that is embedded easily is a DataTable.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structure of a KPFigureView\n",
    "\n",
    "`KPFigureView` is an `api` view that fields a request and turns it into data.\n",
    "Using a common set of functions in the plotting modules allows reusing the same view here, instead of writing a new one for every new plot.\n",
    "\n",
    "The view has a `get` method, that will deals with any HTTP GET requests, and to deal with POST requests, a new `post` method will be required. The `get` method will call `fetch` to grab the data and figure from the database if it exists, and `fetch_fresh` if it does not. It is the code in `fetch_fresh` that interacts with the plotting module code.\n",
    "\n",
    "\n",
    "![KPFigureView](assets/img/KPFigureView.png)\n",
    "\n",
    "The `KPFigureView` is instantiated with the plotting module which becomes `self.module`, and `bokeh.embed` has been imported as `be`.\n",
    "\n",
    "The `save` method saves the data into the `KPFigure` Django model that you can find in `project.api.models`.  \n",
    "\n",
    "from `project.api.views`\n",
    "```\n",
    "    def fetch_fresh(self, KP=None, save=True):\n",
    "        # ...\n",
    "        data = self.module.data(KP)\n",
    "        serdata = self.module.json_safe(data)\n",
    "        fig = self.module.figure(data)\n",
    "        figdata = be.json_item(fig, self.name)\n",
    "        if save:\n",
    "            self.save(self.name, KP,  meta, serdata, figdata)\n",
    "        return figdata\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structure and purpose of model `KPFigure`\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make a new one\n",
    "One of the simpler modules will form a good template with which to begin. If\n",
    "your new module will be similar to an existing one, use that. But I will suggest\n",
    "you use the residual tension module `tension.py` as the base here. It can be\n",
    "found from the root of the project, in the directory `cop_2001`. There is now a\n",
    "`module_template.py` file which can also be used as a base.\n",
    "\n",
    "As you have seen above the minimum we need is three functions:\n",
    "\n",
    "```\n",
    "def data(KP=None):\n",
    "    d = {}\n",
    "    return {}\n",
    "\n",
    "def json_safe(d):\n",
    "    return d\n",
    "\n",
    "def figure(d):\n",
    "    fig = bokeh.plotting.figure()\n",
    "    return fig\n",
    "```\n",
    "\n",
    "#### Fetch `data`\n",
    "\n",
    "Your first task will be to write a fetch function that queries the database and\n",
    "returns your data, perhaps as a dataframe. It is even more common to return a\n",
    "dictionary with the dataframe inside it, so let's do that.\n",
    "\n",
    "You can write this function directly in the `data` function, or call your\n",
    "auxiliary function from 'data'. You will also need a database connection of\n",
    "which you have the choice of two flavours.\n",
    "\n",
    "1. `utils.sea_conn` - a SEADB data connection with SQLAlchemy that comes with a\n",
    "   45s startup penalty. This is probably what you will be familiar with, but can\n",
    "   be painful to use outside of a notebook.\n",
    "2. `utils.pg_conn` - a more traditional postgres connection from the psycopg2\n",
    "   library. This is the same as used under-the-covers by `sea_conn`, uses SQL\n",
    "   and has no startup penalty.\n",
    "\n",
    "```notebook\n",
    "def _fetch(KP=None):\n",
    "    return None\n",
    "```\n",
    "\n",
    "Now make a test suite and run the tests. You will find a template in\n",
    "`tests/module_test_template.py`.\n",
    "\n",
    "#### Make your data `json_safe`\n",
    "\n",
    "Your module needs a function to ensure the data is json safe because that data\n",
    "will be stored in json field of the database. This `safety` could be provided in\n",
    "the `save` method of the Django model, but you know your data best, and keeping\n",
    "the safety function near the functions that make it keeps maintenance easy.\n",
    "\n",
    "Typical difficulties for json serialisation include:\n",
    "- NaN\n",
    "- Numpy datatypes like integers\n",
    "- Dates and datetimes\n",
    "- Numpy arrays\n",
    "- Pandas dataframes\n",
    "- Shapely geometries\n",
    "\n",
    "Usually calling the relevant vanilla datatype will convert, eg.\n",
    "`int(numpy_int)`, or converting to vanilla datastructures eg. `list(array)`, but\n",
    "there are usually corner-cases you will need to be aware of. Again, there may be\n",
    "smarter ways to automatically json safe stuff, but these often end up down the\n",
    "rabbit-hole.\n",
    "\n",
    "You know your data, you know how to convert it, and to convert it back in your\n",
    "`figure` function. If in doubt, you will almost always be able to `str` it, but\n",
    "you may have trouble reconstituting it later.\n",
    "\n",
    "#### Make a bokeh figure\n",
    "\n",
    "Lastly, your module should make a figure and return it.\n",
    "This should be reasonably similar to other modules. I have one suggestion,\n",
    "however, and that is to keep the changes to the data_dict like names, and unit\n",
    "conversion, separate from the plotting code. I typically end up with a function\n",
    "to fetch the data, a function to transform the data, then a function to plot the\n",
    "transformed data, and this had allowed the to relatively reuseable. This is a\n",
    "bot like the fetch, crunch, export workflow of the orcaflex analyses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Troubleshooting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
