# Web Series

Tutorial series for 
* backend
* frontend
* deployment


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fdarkseas%2Fweb-series/master?filepath=demo.ipynb)
